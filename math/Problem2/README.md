2. Greatest common divisor

Write a program that, given two positive integers,
 will calculate and print the greatest common divisor of the two.

풀이:
유클리드 호제법을 사용.
-. 2개의 자연수 또는 정식(整式)의 최대공약수를 구하는 알고리즘.

2개의 자연수 (또는 정식) a, b 에 대해서
 a를 b로 나눈 나머지를 r이라 하면 (단, a>b), a와 b의 최대공약수는 b와 r의 최대공약수와 같다.

이 성질에 따라, b를 r로 나눈 나머지 r'를 구하고,
 다시 r을 r'로 나눈 나머지를 구하는 과정을 반복하여 나머지가 0이 되었을 때 나누는 수가 a와 b의 최대공약수이다.

```
int gcd(int n1, int n2)
{
    int r;
    while (n2 != 0)
    {
        r = n1 % n2;
        n1 = n2;
        n2 = r;
    }
    return n1;
}
```

참고:
호제법이란?
 두 수가 서로(互) 상대방 수를 나누어(除)서 결국 원하는 수를 얻는 알고리즘을 나타낸다.
