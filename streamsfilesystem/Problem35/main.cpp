#include <gsl/gsl>
#include <getopt.h>
#include <dirent.h>
#include <sys/stat.h>
#include <iostream>

using namespace std;

uint64_t totalSize = 0U;
bool mWithSymbolic = false;

void
scanFolder(const string &path) {
    cout << path << endl;
    if (auto dir = opendir(path.c_str())) {
        while (auto f = readdir(dir)) {
            if (f->d_name[0] == '.') {
                continue;
            }
            if (mWithSymbolic && f->d_type == DT_LNK) {
                continue;
            }
            if (f->d_type == DT_DIR) {
                scanFolder(path + f->d_name + "/");
            }
            if (f->d_type == DT_REG) {
                string name = f->d_name;
                struct stat st;
                if(stat((path + name).c_str(), &st) == 0) {
                    totalSize += st.st_size;
                }
            }
        }
        closedir(dir);
    }
}


int main(int argc, char* argv[])
{
    int32_t opt;

    const char *path;
    while ((opt = getopt(argc, argv, "d:s")) != -1) {
        switch (opt) {
            case 's':
                mWithSymbolic = true;
                break;
            case 'd':
                path = optarg;
                break;
            default: /* '?' */
                fprintf(stderr, "Usage: %s [-s]\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

//    printf("path: %s\nwithSymbolic=%d\n optind=%d\n", path, mWithSymbolic, optind);

    string sPath(path);
    scanFolder(sPath);
    cout << totalSize << endl;

    return 0;

}
