#include <gsl/gsl>
#include <fstream>
#include <iostream>
#include <ostream>
#include <vector>

using namespace std;

int main(int argc, char* argv[])
{
    const char *oldFile = argv[1];
    const char *newFile = argv[2];
    ifstream input(oldFile);
    ofstream output(newFile, ios::out | ios::trunc);

    string line;
    std::vector<string> buffers ;
    while (getline(input, line)) {
        if (!line.empty()) {
            cout << line << ": " << line.size() << endl;
            buffers.push_back(line);
        }
    }
    for (auto buf : buffers) {
        output << buf << '\n';
    }
    remove(oldFile);
    rename(newFile, oldFile);
    return 0;
}
