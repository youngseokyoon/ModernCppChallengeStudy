#include <gsl/gsl>
#include <iostream>
#include <vector>

using namespace std;

vector<int32_t> getP(vector<int32_t> number) {
    uint32_t size = number.size();
    vector<int32_t> newNumber;
    vector<int32_t>::iterator newIt = newNumber.begin();
    vector<int32_t>::iterator it;

    for (it = number.begin(); it != number.end(); it++) {
        int32_t total = *it;
        if (!(newIt == newNumber.begin())) {
            total += *(it - 1);
        }

        newNumber.push_back(total);
    }
    it = number.end() - 1;
    newNumber.push_back(*it);
    return newNumber;
}

int main(int argc, char* argv[])
{
    int32_t count = atoi(argv[1]);
    vector<int32_t> v1 = {1};
    for (int i = 0; i < count - 1; i++) {
        v1 = getP(v1);
    }
    for (auto s : v1) {
        cout << " " << s;
    }
    cout << endl;
    return 0;
}
