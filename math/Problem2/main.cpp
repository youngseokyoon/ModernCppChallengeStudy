#include <gsl/gsl>

using namespace std;

// using Euclidean algorithm
int gcd(int n1, int n2)
{
    int r;
    while (n2 != 0)
    {
        r = n1 % n2;
        n1 = n2;
        n2 = r;
    }
    return n1;
}

int main(int argc, char* argv[])
{
    int32_t n1 = atoi(argv[1]);
    int32_t n2 = atoi(argv[2]);

    printf("%d \n", gcd(n1, n2));
    return 0;
}
