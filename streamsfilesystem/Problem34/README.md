34. Removing empty lines from a text file 

Write a program that, given the path to a text file, 
 modifies the file by removing all empty lines. 

Lines containing only whitespaces are considered empty.
